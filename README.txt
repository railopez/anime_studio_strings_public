**************************************************
The Spanish Anime Studio Pro strings repository
**************************************************


**************************************************
About...
**************************************************
A place where make corrections and discuss about quality of the Anime Studio's "Strings.ES.txt" file.


**************************************************
Translation Conventions
**************************************************
1. Literalness is important, but I think UI space and naturalness are more important aspects. Anyway, there should be always a certain balance between this three involved aspects.
 
2. From my experience, I can say "Cinema 4D" and "After Effects" (or, although not so closely related, "Photoshop") Spanish translations are like a very good source of inspiration.


**************************************************
Repository Conventions
**************************************************
1. Simple 'word' or 'words' corrections will be commited as:

	- 'Old word' -> 'New word'. 'Another old word' -> 'Another new word'
	- A commit for each correction is desirable, but if there were more simple corrections, they'll be separated by a period.
	- Of course, other kind of corrections can have different commit message conventions.


2. Ideally, I think a "per build branch" scenario would be the way to go (under consideration).


**************************************************
Notes
**************************************************
- IMPORTANT (for beta testers): New features' related strings (those under NLA) will never be part of the puplic repository but of a private clone of it, in order to avoid new features spreading.

- Ideally, intricate words or "controversial" pieces of translation will be previously discussed for consensus. It can be done anywhere, but preferably making use of the repository's bug tracker section or the following AS Forum's existing post:

	http://www.lostmarble.com/forum/viewtopic.php?f=18&t=24560

